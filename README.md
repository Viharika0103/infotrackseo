# Dev Environment Setup
- Install dotnetcore 3.1: https://www.microsoft.com/net/core#windows
- Install visual studio 2019 : https://visualstudio.microsoft.com/downloads/


# Download and install all packages needed for front end
$ npm install

# Download required .Net libraries and build the code base
$ dotnet clean
$ dotnet restore
$ dotnet build


# links used to get the SEO for both search engine:

https://infotrack-tests.infotrack.com.au/Google/Page{01-10}.html 
https://infotrack-tests.infotrack.com.au/Bing/Page{01-10}.html

{01 - 10} should be passed individually like Page01, Page02 ...

# App URL
http://localhost:5000/
Can be changed in solution 
Right click the project  properties  Debug in the left pane  Web Server Setting in right pane App URL
