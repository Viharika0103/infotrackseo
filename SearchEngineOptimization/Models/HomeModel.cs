﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SearchEngineOptimization.Models
{
    public class SEO
    {
        public List<SearchRange> Search { get; set; }
    }

    public class SearchRange
    {
        public string Range { get; set; }
        public string Searchengine { get; set; }
    }
}
