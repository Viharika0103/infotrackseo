﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using HtmlAgilityPack;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using SearchEngineOptimization.Models;


namespace SearchEngineOptimization.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        // load snippet
        HtmlAgilityPack.HtmlDocument htmlSnippet = new HtmlAgilityPack.HtmlDocument();

        public HomeController(ILogger<HomeController> logger)
        {
            _logger = logger;
        }

        public IActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public JsonResult GetRanking(int range, string searchengine)
        {
             _logger.LogInformation("Get Ranking from SEO" + searchengine);

            string ranking = getinfotracksearch(range , searchengine);

           
            return new JsonResult(ranking);        

        }

        
        private string getinfotracksearch(int searchrange, string searchengine)
        {
            //count of url having http and https
            int urlcount = 0;

            //all the url with word infotract
            List<int> ranklist = new List<int>();

            
            // inorder to search each page of SEO incrementing and passing the page number dynamically
            for (int i = 1; i <= 10; i++)
            {
                if (urlcount <= searchrange) // to search for the range
                {

                    _logger.LogInformation(urlcount.ToString());

                    string SearchResults = "";
                    StringBuilder sb = new StringBuilder();
                    byte[] ResultsBuffer = new byte[8192];

                    // as the page numbers in the search links are prefixed by 0 as default for single digits
                    if (i <= 9)
                    {
                        SearchResults = "https://infotrack-tests.infotrack.com.au/" + searchengine + "/Page0" + i + ".html";
                    }
                    else
                    {
                        SearchResults = "https://infotrack-tests.infotrack.com.au/" + searchengine + "/Page" + i + ".html";
                    }
                    HttpWebRequest request = (HttpWebRequest)WebRequest.Create(SearchResults);
                    HttpWebResponse response = (HttpWebResponse)request.GetResponse();

                     _logger.LogInformation(response.ToString());

                    Stream resStream = response.GetResponseStream();
                    string tempString = null;
                    int count = 0;
                    do
                    {
                        count = resStream.Read(ResultsBuffer, 0, ResultsBuffer.Length);
                        if (count != 0)
                        {
                            tempString = Encoding.ASCII.GetString(ResultsBuffer, 0, count);
                            sb.Append(tempString);
                        }
                    }

                    while (count > 0);
                    string LinksHTML = sb.ToString();

                    //HTML of the search engine requested
                    _logger.LogInformation(LinksHTML);

                    // ExtractingLinks                
                    if (LinksHTML.Contains("<a href=") || LinksHTML.Contains(""))
                    {

                        var matches = (Regex.Matches(LinksHTML, "<a href=(.*?) ping=|<a href=(.*?) h="));

                        foreach (Match item in matches)
                        {
                            string node = item.ToString();
                            //   Console.WriteLine(text);  // outputs HtmlAgilityPack.HtmlNode instead o th actual link!!

                            string link = node.ToString();

                            if (link.ToString().Contains("www.") && link.ToString().Contains("http://") || link.ToString().Contains("https://"))
                            {
                                urlcount++; // to get the the ranking of the URL
                                if (urlcount <= searchrange) // to display requested search range 
                                {
                                    if (link.ToString().Contains("infotrack"))
                                    {
                                        ranklist.Add(urlcount); // get the rank of the URL with www.http:// or www.https://infortract....

                                    }
                                }

                            }


                        }

                    }
                }
            }
            return string.Join(",", ranklist.Select(n => n.ToString()).ToArray());
        }

    }
}
